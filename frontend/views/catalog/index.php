<?php
/**
 * @var yii\web\View $this
 * @var array $categories
 * @var common\models\Product[] $products
 * @var common\models\Category|null $category
 */
use yii\helpers\Url;

//TODO: запилить картинки товаров
?>
<div class="row">
    <div class="col-md-3">
        <div class="nav">
            <h3>Списк категорий</h3>
            <?= $this->render('topics', [
                'categories' => $categories
            ]) ?>
        </div>
    </div>
    <div class="col-md-9">
        <h1><?= ($category ? $category->title : 'Каталог товаров') ?></h1>
        <?php if (!empty($products)): ?>
            <table class="table table-bordered table-hover">
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td>image</td>
                        <td>
                            <div class="title"><a
                                    href="<?= Url::to(['product/view', 'id' => $product->id]) ?>"><?= $product->title ?></a>
                            </div>
                            <div class="text"><?= $product->description ?></div>
                            <div class="price"><?= $product->price ?> рублей</div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php else: ?>
            <div class="error">Здесь товаров нет вообще!</div>
        <?php endif; ?>
    </div>
</div>

