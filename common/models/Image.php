<?php

namespace common\models;

use Yii;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $product_id
 *
 * @property Product $product
 * @property string $src
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile $_image
     */
    public $_image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['product_id'], 'exist', 'targetClass' => 'common\models\Product', 'targetAttribute' => 'id'],
            ['_image', 'image', 'maxSize' => 1024 * 1024 * 3,
                'extensions' => ['jpg', 'jpeg'],
                'mimeTypes' => ['image/jpeg'], 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }


    public function afterSave($insert, $changedAttributes)
    {
        //FIXME: целостность нарушилась. ID нельзя получить раньше сохранения
        if ($this->_image) {
            $this->saveImage();
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    protected function getPath()
    {
        $path = Yii::getAlias(Yii::$app->params['static']['images']['path']);

        return "{$path}/{$this->product_id}.jpg";
    }

    protected function getWebPath()
    {
        $path = Yii::getAlias(Yii::$app->params['static']['images']['web']);

        return "{$path}/{$this->product_id}.jpg";
    }

    protected function saveImage()
    {
        $file_saved = $this->_image->saveAs(static::getPath());

        return $file_saved;
    }

    public function getSrc()
    {
        return self::getWebPath();
    }
}
